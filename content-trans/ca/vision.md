---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Visió
    parent: project
    weight: 1
title: La nostra visió
---
El Plasma Mobile té com a objectiu arribar a ser un sistema complet i de programari lliure per als dispositius mòbils.<br /> Està dissenyat per donar als usuaris conscients de la privacitat el control sobre la seva informació i la comunicació.

El Plasma Mobile té un enfocament pragmàtic i és inclusiu per al programari de tercers, permetent que l'usuari triï les aplicacions i serveis a usar, mentre proporciona un experiència transparent entre múltiples dispositius.<br /> El Plasma Mobile implementa estàndards oberts i està desenvolupat en un procés transparent que està obert a la participació de tothom.
