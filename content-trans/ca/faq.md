---
layout: page
menu:
  main:
    name: PMF
    parent: project
    weight: 5
title: Preguntes més freqüents
---
Perquè el Plasma Mobile no usa Mer/Nemomobile?
----------------------------------------------

El Plasma Mobile és una plataforma de programari per a dispositius mòbils. Per si mateix no és cap sistema operatiu, i consisteix de les Qt5, els Frameworks de KDE, el Plasma i programari variat que és part del conjunt d'aplicacions. El Plasma Mobile pot funcionar sobre la distribució Mer, però degut a la manca de temps i recursos, actualment s'està enfocant en el KDE Neon per al PinePhone com a base per al desenvolupament i les proves.

Les aplicacions Android funcionen en el Plasma Mobile?
---------------------------------------

Hi ha projectes com l'[Anbox](https://anbox.io/) que és un Android executant-se dins un contenidor Linux, i usa el nucli de Linux per a executar aplicacions per aconseguir un rendiment gairebé natiu. Això es podria potenciar en el futur per tenir aplicacions Android executant-se sobre d'un sistema GNU/Linux amb la plataforma Plasma Mobile, però és una tasca complicada, i a data d'*avui* (6 de setembre de 2020) diverses distribucions ja admeten l'Anbox i es pot executar el Plasma Mobile en aquestes distribucions.

Puc executar el Plasma Mobile en el meu dispositiu mòbil?
--------------------------------------------

Actualment, el Plasma Mobile funciona en els tipus de dispositius següents:

* **(Recomanat) PinePhone:** Oferim imatges oficials construïdes per al PinePhone sobre del KDE Neon. Podeu trobar més informació a la [documentació](https://docs.plasma-mobile.org) del Plasma Mobile.

* **Basat en x86:** Si voleu provar el Plasma Mobile en una tauleta, ordinador/portàtil Intel, o una màquina virtual, la [imatge](https://www.plasma-mobile.org/ca/get/) del Plasma Mobile basada en Neon x86_64 és per a vós. Es pot trobar informació quant a instal·lar-la permanentment a la [documentació](https://docs.plasma-mobile.org) del Plasma Mobile.

* **Dispositius postmarketOS:** El postmarketOS és un Alpine Linux tàctil optimitzat i preconfigurat que es pot instal·lar en els telèfons intel·ligents Android i altres dispositius mòbils. Aquest projecte està en *etapes molt primerenques de desenvolupament* però actualment permet l'ús d'una àmplia varietat de dispositius, i ofereix el Plasma Mobile com una interfície disponible. Cerqueu el vostre dispositiu a la [llista de dispositius acceptats](https://wiki.postmarketos.org/wiki/Devices) i vegeu què funciona, després podreu seguir la [guia d'instal·lació del pmOS](https://wiki.postmarketos.org/wiki/Installation_guide) per a instal·lar-lo en el vostre dispositiu. La vostra experiència pot variar per a cada cas en particular, i **no** necessàriament serà representativa de l'estat actual del Plasma Mobile.

* **Altres:** Desafortunadament, el funcionament dels dispositius basats en Halium s'ha retirat fa poc (vegeu (deute tècnic)[/2020/12/14/plasma-mobile-technical-debt.html]). Això inclou el dispositiu de referència anterior Nexus 5x.

He instal·lat el Plasma Mobile, quina és la contrasenya de connexió?
---------------------------------------------------------

Si heu instal·lat el Neon en el PinePhone mitjançant l'script d'instal·lació, la contrasenya hauria de ser «1234», i es pot canviar després d'executar «passwd» en el Konsole. Per al Manjaro és 123456. En canviar-la, tingueu present que actualment només es poden introduir nombres a la pantalla de bloqueig.

Si esteu usant la imatge x86, no s'estableix cap contrasenya de manera predeterminada, i la podreu establir executant «passwd» en el Konsole abans que us autentifiqueu per a qualsevol cosa.

Quin és l'estat del projecte?
--------------------------------

Actualment el Plasma Mobile està en desenvolupament intens i no està destinat a un ús diari. Si esteu interessat/da en col·laborar-hi, [uniu-vos](/findyourway) al joc.
