---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Vizija
    parent: project
    weight: 1
title: Naša vizija
---
Cilj Plasma Mobile je postati celovit in odprt sistem programske opreme za mobilne naprave.<br />Zasnovan je tako, da uporabnikom, ki se zavedajo zasebnosti, povrne nadzor nad njihove informacije in komunikacije.

Plasma Mobile ima pragmatičen pristop in vključuje programsko opremo tretjih oseb, kar uporabniku omogoča, da izbere, katere programe in storitve naj uporabi, hkrati pa zagotavlja nemoteno izkušnjo na več napravah.<br />Plasma Mobile je izvedena po odprtih standardih in je razvita v preglednem postopku v katerem lahko sodelujejo vsi.
