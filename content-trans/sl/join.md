---
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
- scss/join.scss
title: Pridružite se Plasma Mobile
---
Če želite prispevati k čudoviti prosti programski opremi za mobilne naprave, [se nam pridružite - vedno imamo nalogo za vas] (/contributing/)!

Skupine in kanali skupnosti Plasma Mobile:

### Specifični kanali za Plasma Mobile:

* [![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasmamobile:matrix.org)

* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)

* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)

* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)


### Kanali, ki se nanašajo Plasma Mobile:

* [![](/img/irc.png)#plasma on Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)

* [![](/img/telegram.svg)Halium on Telegram](https://t.me/Halium)

* [![](/img/mail.svg)Plasma development mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel)
