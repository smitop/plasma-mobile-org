---
layout: page
menu:
  main:
    name: FAQ
    parent: project
    weight: 5
title: Perguntas Mais Frequentes
---
Porque é que o Plasma Mobile não está a usar o Mer/Nemomobile?
----------------------------------------------

O Plasma Mobile é uma plataforma de aplicações para dispositivos móveis. Não é um sistema operativo em si, mas consiste no Qt5, nas Plataformas do KDE, no Plasma e em diversas aplicações que fazem parte do conjunto de ferramentas. O Plasma Mobile consegue correr sobre a distribuição Mer, mas devido a uma falta de tempo e recursos, estamos a focar-nos de momento no KDE Neon no PinePhone como base para testes e desenvolvimento.

Será que as aplicações Android funcionam no Plasma Mobile?
---------------------------------------

Existem projectos como o [Anbox](https://anbox.io/), que é o Android a executar-se dentro de um contentor de Linux, usando o 'kernel' do Linux para executar as aplicações, de forma a ter uma performance próxima da nativa. Isto pode ser ajustado no futuro para ter as aplicações de Android a correr sobre um sistema GNU/Linux com a plataforma Plasma Mobile, mas é uma tarefa complicada, pelo que à data de *hoje*(6 de Setembro de 2020), algumas distribuições já têm suporte para o Anbox e consegue executar o Plasma Mobile sobre essas distribuições.

Posso executar o Plasma Mobile no meu dispositivo móvel?
--------------------------------------------

De momento, o Plasma Mobile funciona nos seguintes tipos de dispositivos:

* **(Recomendado) PinePhone:** Fornecemos imagens oficiais compiladas para o PinePhone no topo do KDE Neon. Poderá procurar mais informações sobre a [documentação](https://docs.plasma-mobile.org) do Plasma Mobile.

* **baseados em x86:** Se quiser experimentar o Plasma Mobile num 'tablet' Intel, em computadores/portáteis ou máquinas virtuais, uma imagem do Plasma Mobile baseado em Neon para x86_64 [aqui](https://www.plasma-mobile.org/get/) para si. Poderá encontrar informações sobre a instalação permanente na [documentação](https://docs.plasma-mobile.org) do Plasma Mobile.

* **Dispositivos postmarketOS:** o postmarketOS é um Alpine Linux optimizado para dispositivos tácteis e pré-configurado que poderá ser instalado em telemóveis Android e noutros dispositivos móveis. Este projecto está ainda numa *fase muito prévia do desenvolvimento*, mas já oferece de momento o suporte para uma gama relativamente abrangente de dispositivos, oferecendo o Plasma Mobile como interface disponível. Por favor procure o seu dispositivos na [lista de dispositivos suportados](https://wiki.postmarketos.org/wiki/Devices) e ver o que está a funcionar, podendo então seguir o [manual de instalação do pmOS](https://wiki.postmarketos.org/wiki/Installation_guide) para o instalar no seu dispositivo. A sua experiência poderá variar, e **não** representa necessariamente o estado actual do Plasma Mobile.

* **Outros:** Infelizmente o suporte para os dispositivos baseados em Halium teve de ser descontinuado recentemente (ver as (limitações técnicas)[/2020/12/14/plasma-mobile-technical-debt.html]). Isto inclui o anterior dispositivo de referência, o Nexus 5x.

Instalei o Plasma Mobile, qual é a senha da conta?
---------------------------------------------------------

Se tiver instalado o  Neon no seu PinePhone através do programa de instalação, a senha deverá ser "1234", podendo alterá-la depois se executar o "passwd" no Konsole. Para o Manjaro é 123456. Ao modificá-la, por favor tenha em mente que só conseguirá introduzir números no ecrã de bloqueio.

Se estiver a usar a imagem para x86, não está nenhuma senha definida por omissão, pelo que terá de a definir com o comando "passwd" no Konsole antes de se poder autenticar seja para o que for.

Qual é o estado do projecto?
--------------------------------

O Plasma Mobile está de momento em desenvolvimento intenso e não pretende ser usado como base diária. Se estiver interessado em contribuir, [junte-se](/findyourway) ao jogo.
