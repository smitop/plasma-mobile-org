---
jsFiles:
- https://cdn.kde.org/aether-devel/version/kde-org/applications.3e16ae06.js
layout: page
menu:
  main:
    parent: project
    weight: 2
sassFiles:
- scss/screenshots.scss
- scss/components/swiper.scss
screenshots:
- name: Plasma mobile homescreen
  url: /screenshots/plasma.png
- name: KWeather, Plasma mobile weather application
  url: /screenshots/weather.png
- name: Kalk, a calculator application
  url: /screenshots/pp_calculator.png
- name: Megapixels, a camera application
  url: /screenshots/pp_camera.png
- name: Calindori, a calendar application
  url: /screenshots/pp_calindori.png
- name: KClock
  url: /screenshots/pp_kclock.png
- name: Buho, a note taking application
  url: /screenshots/pp_buho.png
- name: Kongress
  url: /screenshots/pp_kongress.png
- name: Okular Mobile, a universal document viewer
  url: /screenshots/pp_okular01.png
- name: Angelfish, a web browser
  url: /screenshots/pp_angelfish.png
- name: Nota, a text editor
  url: https://nxos.org/wp-content/uploads/2020/11/nota_1_2-min-1024x751.png
- name: Pix, another image viewer
  url: https://nxos.org/wp-content/uploads/2020/11/pix-1024x805.png
- name: Index, a file manager
  url: /screenshots/pp_folders.png
- name: VVave, a music player
  url: https://nxos.org/wp-content/uploads/2020/11/vvave-1024x724.png
- name: The hardware
  url: /screenshots/20201110_092718.jpg
title: Obrázky
---
The following screenshots were taken from a Pinephone device running Plasma Mobile.

{{< screenshots name="screenshots" >}}
