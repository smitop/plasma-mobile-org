---
konqi: /img/mascot_20140702_konqui-plasma_wee_0.png
layout: page
menu:
  main:
    name: Visione
    parent: project
    weight: 1
title: La nostra visione
---
Plasma Mobile aspira a diventare un sistema software completo e aperto per dispositivi mobili.<br />È progettato per ridare agli utenti attenti alla riservatezza il controllo sulle loro informazioni e comunicazioni.

Plasma Mobile adotta un approccio pragmatico ed è comprensivo di software di terze parti, permettendo all'utente di scegliere quali applicazioni e servizi utilizzare e fornendo un'esperienza senza interruzioni su più dispositivi.<br /> Plasma Mobile implementa standard aperti, ed è sviluppato in un processo trasparente che è aperto per chiunque voglia parteciparvi.
