---
layout: page
menu:
  main:
    name: FAQ
    parent: project
    weight: 5
title: Domande frequenti
---
Perché Plasma Mobile non usa Mer/Nemomobile?
----------------------------------------------

Plasma Mobile è una piattaforma software per i dispositivi mobili. Non è un sistema operativo in sé: è composto da Qt5, da KDE Frameworks, da Plasma e da software vario che è parte dell'insieme di applicazioni. Plasma Mobile può funzionare sopra alla distribuzione Mer ma, a causa della mancanza di tempo e di risorse, come base per le prove e per lo sviluppo ci stiamo focalizzando su KDE Neon su PinePhone.

Le applicazioni di Android funzionano in Plasma Mobile?
---------------------------------------

Ci sono progetti come [Anbox](https://anbox.io/), che è Android in esecuzione all'interno di un contenitore Linux e utilizza il kernel Linux per eseguire applicazioni per ottenere prestazioni quasi native. Questo potrebbe essere sfruttato in futuro per avere applicazioni Android in esecuzione su un sistema GNU/Linux con la piattaforma Plasma Mobile, ma è complicato. Ad *oggi*, 6 settembre 2020, alcune distribuzioni supportano già Anbox: puoi eseguire Plasma Mobile in queste distribuzioni.

Posso eseguire Plasma Mobile sul mio dispositivo mobile?
--------------------------------------------

Attualmente Plasma Mobile funziona sui seguenti tipi di dispositivi:

* **(Raccomandato) PinePhone:** offriamo immagini ufficiali create per PinePhone in KDE Neon. Puoi trovare maggiori informazioni nella [documentazione](https://docs.plasma-mobile.org) di Plasma Mobile.

* **Basati su x86:** se vuoi provare Plasma Mobile su un tablet, su un computer fisso o portatile Intel o su una macchina virtuale, l'[immagine](https://www.plasma-mobile.org/get/) x86_64 di Plasma Mobile basata su Neon è quella che fa per te. Le informazioni su come installarla permanentemente possono essere trovate nella [documentazione](https://docs.plasma-mobile.org) di Plasma Mobile.

* **Dispositivi con postmarketOS:** postmarketOS (pmOS) è un Alpine Linux ottimizzato per il tocco e preconfigurato che può essere installato su smartphone e su altri dispositivi mobili. Il progetto è *nelle primissime fasi di sviluppo* ma attualmente offre il supporto a una discreta gamma di dispositivi, e offre anche Plasma Mobile tra le interfacce disponibili. Cerca il tuo dispositivo nell'[elenco dei dispositivi supportati](https://wiki.postmarketos.org/wiki/Devices) e vedi che cosa funziona, poi potrai seguire la [guida all'installazione di pmOS](https://wiki.postmarketos.org/wiki/Installation_guide) per installarlo sul tuo dispositivo. La tua esperienza potrebbe essere diversa e **non** essere necessariamente rappresentativa dello stato attuale di Plasma Mobile.

* **Altri:** sfortunatamente il supporto per i dispositivi basati su Halium è stato di recente abbandonato (vedi il (debito tecnico)[/2020/12/14/plasma-mobile-technical-debt.html]). Questo include il dispositivo di riferimento precedente, il Nexus 5x.

Ho installato Plasma Mobile, qual è la password di accesso?
---------------------------------------------------------

Se hai installato Neon sul PinePhone con lo script d'installazione, la password dovrebbe essere «1234», ma può essere modificata subito dopo eseguendo «passwd» in Konsole. Per Manjaro è 123456. Se la cambi, ricordati che nella schermata di blocco potrai inserire solo dei numeri.

Se stai usando l'immagine x86, per impostazione predefinita non è stata impostata nessuna password: devi farlo eseguendo «passwd» in Konsole prima di poterti autenticare per qualsiasi cosa.

Qual è lo stato del progetto?
--------------------------------

Plasma Mobile è attualmente in una fase di sviluppo intenso, e non è concepito per essere utilizzato come dispositivo principale. Se sei interessato a contribuire, [join](/findyourway) the game.
