---
layout: page
menu:
  main:
    name: 常见问题
    parent: project
    weight: 5
title: 常见问题解答
---
为什么 Plasma Mobile 不采用 Mer/Nemomobile？
----------------------------------------------

Plasma Mobile 是一款移动设备的软件平台，它本身不是一个操作系统，它由 Qt5、 KDE 程序框架、 Plasma 和它的各种配套应用组成。Plasma Mobile 可以在 Mer 发行版下面工作，但由于时间和资源所限，我们目前只能专注于将 PinePhone 上运行的 KDE Neon 发行版作为测试和开发的基础系统。

Android 应用能在 Plasma Mobile 中工作吗？
---------------------------------------

我们建议您关注 [Anbox](https://anbox.io/) 项目，它是一个运行在 Linux 容器内的安卓系统，使用 Linux 内核来执行应用并能实现接近原生系统的性能。我们或许可以在 Plasma Mobile 运行的 GNU/Linux 操作系统中使用 Anbox 来支持安卓应用，不过这并不是一个简单的任务。不过在本文写作的 2020 年 9 月，已经有许多发行版支持 Anbox，而您可以在这些发行版中运行 Plasma Mobile。

我可以在我的手机上运行 Plasma Mobile 吗？
--------------------------------------------

目前 Plasma Mobile 能够在下列设备中运行：

* **PinePhone (Recommended)：**我们提供面向 PinePhone 构建的 KDE Neon 官方镜像，详情参见 Plasma Mobile [文档](https://docs.plasma-mobile.org)。

* **x86 设备：**如果您想在一台 Intel 平板/桌面/笔记本电脑/虚拟机上试用 Plasma Mobile，您可以使用基于 KDE Neon x86_64 版的 Plasma Mobile [镜像](https://www.plasma-mobile.org/get/)。安装指南请见[文档网站](https://docs.plasma-mobile.org)。

* **postmarketOS 设备：** postmarketOS 是一款面向触摸优化、预先配置的 Alpine Linux 发行版，可以安装到安卓智能手机和其他移动设备。该项目仍处于早期开发阶段，但目前已经支持相当数目的设备，且提供 Plasma Mobile 作为可用界面选项。您可在[该项目的支持设备列表](https://wiki.postmarketos.org/wiki/Devices)中找到您的设备，确认它的支持状态。然后您可以根据 [pmOS 安装指南](https://wiki.postmarketos.org/wiki/Installation_guide) 将它安装到设备中。具体的使用体验因设备而异，并且它并不能反映 Plasma Mobile 的最新状态。

* **其他：**基于 Halium 设备的支持工作已被放弃 (请查看 (相关文章)[/2020/12/14/plasma-mobile-technical-debt.html])。项目先前的参考设备 Nexus 5x 的支持也已停止。

我已经安装了 Plasma Mobile，它的登录密码是什么？
---------------------------------------------------------

如果您使用安装脚本将 KDE Neon 安装到了 PinePhone，初始密码是“1234”，您可以在 Konsole 终端中运行“passwd”指令更改密码。Manjaro 系统的初始密码则是“123456”。在更改密码时，请记得目前您只能在锁屏界面上输入数字。

如果您安装的是 x86 版镜像，那么将不会有任何初始密码。您需要先在 Konsole 中运行“passwd”指令设置一个密码，否则您将无法对需要权限的任何操作进行身份认证。

Plasma Mobile 项目的发展状态如何？
--------------------------------

Plasma Mobile 目前仍然处于快速迭代的前期研发阶段，不适合用作日常系统。如果您有兴趣为它贡献力量，请[加入我们](/findyourway)。
