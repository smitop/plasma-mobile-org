---
author: Jonah Brüchert
created_at: 2018-12-28 13:30:00 UTC+1
date: "2018-12-28T00:00:00Z"
title: New documentation
---
<figure style="border: 1px solid grey; text-align: center;">
<a href="/img/docs-plasma-mobile-org.png"><img src="/img/docs-plasma-mobile-org.png"></a>
</figure>


Today we released our new documentation website, which can be found on [docs.plasma-mobile.org](https://docs.plasma-mobile.org).
The new documentation is based on sphinx and replaces our part of the KDE community wiki.

All existing content from the wiki has been migrated, and can be further edited and improved by sending merge requests to the [repository](https://invent.kde.org/websites/docs-plasma-mobile-org) on the KDE GitLab instance.

Thanks to the KDE VDG team and to ReadTheDocs for creating the sphinx design we are using!
