---
title: Join Plasma Mobile
konqi: /img/424px-Mascot_konqi-app-internet.png
menu:
  main:
    parent: community
    weight: 2
sassFiles:
  - scss/join.scss
---

If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](/contributing/)!

Plasma Mobile community groups and channels:

### Plasma Mobile specific channels:

* [![](/img/matrix.svg)Matrix (most active)](https://matrix.to/#/#plasmamobile:matrix.org) 

* [![](/img/telegram.svg)Telegram](https://t.me/plasmamobile)

* [![](/img/irc.png)IRC](https://kiwiirc.com/nextclient/chat.freenode.net/#kde-plasmamobile)

* [![](/img/mail.svg)Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)


### Plasma Mobile related project channels:

* [![](/img/irc.png)#plasma on Freenode (IRC)](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)

* [![](/img/telegram.svg)Halium on Telegram](https://t.me/Halium)

* [![](/img/mail.svg)Plasma development mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel)
